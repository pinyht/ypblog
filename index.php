<?php
/*
 * ------------------------------------------------------------
 * 系统入口文件
 *
 * 功能说明: 进行系统初始化和模块路由
 * 作者: ypblog
 * 版本: v0.1
 * 创建日期: 2012-04-20
 * 更新日期: 2012-05-08
 * 版权信息: Copyright (c) 2012, pinyht   
 * ------------------------------------------------------------
 */

// 加载基础配置文件
require_once('config/config.common.php');

// 加载数据库相关配置
require_once('config/config.mysql.php');

// 加载类文件自动包含函数
require_once('kernel/class/init_class.php');

// 初始化pdo对象
$pdo = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASSWORD);

$a = new Template();
$a->SetDelimiter('<{', '}>');
$a->SetTemplateDir(ROOT_PATH.'/template');
$a->SetTheme('default');

$a->Assign('varA', '"333"');
$a->Assign('varB', 'aasf');
$a->Assign('varC', 
    array(
        "a" => "测试1", 
        "b" => "测试2"
    )
);
$a->Assign('varD', 
    array(
        "adfasf" => array("a" => "标题1"), 
        "bbb" => array("a" => "标题2")
    )
);
echo 'aaa';exit;
$a->Display('index.html');
exit;
print_r($pdo->query('select * from user')->fetch());exit;
echo gettype($pdo);exit;

?>
