<?php
/*
 * ------------------------------------------------------------
 * 基础配置文件
 *
 * 功能说明: 系统基础配置文件,系统相关的基础配置在此进行定义
 * 作者: ypblog
 * 版本: v0.1
 * 创建日期: 2012-04-20
 * 更新日期: 2012-04-20
 * 版权信息: Copyright (c) 2012, pinyht   
 * ------------------------------------------------------------
 */

/*
 * 设置系统错误报告
 *
 * 参数说明: 
 *      error_reporting -- 设置错误报告的级别
 *      E_ALL -- E_STRICT除外的所有错误和警告信息         
 *      E_STRICT -- 启用PHP对代码的修改建议,以确保代码具有最佳的互操作性和向
 *                  前兼容性
 *      E_NOTICE -- 运行时通知,表示脚本遇到可能会表现为错误的情况,但是在可以
 *                  正常运行的脚本里面也可能会有类似的通知
 */

ini_set('error_reporting',E_ALL);
//跟踪除注意信息之外的所有错误
//ini_set('error_reporting',E_ALL & ~E_NOTICE);  

/*
 * 关闭屏幕显示错误
 *
 * 参数说明:
 *      display_errors -- 该选项设置是否将错误信息作为输出的一部分显示到屏
 *                        幕,或者对用户隐藏而不显示,该配置值的类型为boolean
 */

ini_set('display_errors',false);

/*
 * 开启错误日志
 *
 * 参数说明: 
 *      log_errors -- 设置是否将脚本运行的错误信息记录到服务器错误日志或
 *                    者error_log之中,该配置值的类型为boolean
 */

ini_set('log_errors',true);

/*
 * 设置所有脚本的输出编码为utf-8
 * header函数用来发送一个自定义的HTTP头信息
 */

header('Content-Type: text/html; charset=utf-8');

/*
 * 定义站点根目录的绝对路径
 *
 * 函数说明: 
 *      dirname -- 返回路径中的目录部分
 * 参数说明:
 *      __FILE__ -- 文件的完整路径和文件名,魔术常量
 */

define('ROOT_PATH', str_replace('/config', '', dirname(__FILE__)));

/*
 * 定义站点的网站根路径URL
 *
 * 参数说明:
 *      $_SERVER['SERVER_NAME'] -- 当前url站点地址
 */

define('SITE_PATH', 'http://'.$_SERVER['SERVER_NAME'].'/ypblog');

?>
