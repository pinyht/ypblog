<?php
/*
 * ------------------------------------------------------------
 * 模板类定义文件
 *
 * 作者: ypblog
 * 创建日期: 2012-04-20
 * 更新日期: 2012-05-03
 * 版权信息: Copyright (c) 2012, pinyht   
 * ------------------------------------------------------------
 */

/*
 * 模板类
 *
 * 功能说明: 模板引擎核心类,负责将程序数据输出至模板文件,对模板语言进行解析
 * 示例:
 *      1.创建模板对象
 *        $obj = new Template();
 *        $obj->SetDelimiter('<{', '}>');     // 自定义边界符
 *        $obj->SetTemplateDir('/template');  // 定义模板路径
 *        $obj->SetTheme('default');          // 定义主题
 *
 *      2.向模板对象收集数据
 *        $obj->Assign('varA', '1111');     // 此时模板对象拥有一个变量名
 *                                          // 为varA的数据 
 *        $obj->Assign('varB', array('a', 'b', 'c'));   // varB的值为数组
 *        $obj->Assign('varC', array(
 *                array(
 *                    'a' => 'value1',
 *                    'b' => 'value2'
 *                )
 *            )
 *        );
 *
 *      3.将数据发送至模板文件并解析模板语句
 *        $obj->Display('index.html');
 *
 *      4.模板注释语句
 *        <{*注释部分*}>
 *
 *      5.模板变量
 *        <{$varA}>
 *
 *      6.模板include语句
 *        <{include('footer.html')}>
 *
 *      7.模板foreach语句
 *        <{foreach $varD as $b}>
 *            <span><{$b.a}></span>
 *            <span>asdfasdf</span>
 *            <span>asddafsdafasdf</span>
 *        <{endforeach}>
 *
 *      8.模板if语句
 *        说明:不支持elseif
 *              eq -- 等于
 *              ne或者neq -- 不等于
 *              gt -- 大于
 *              lt -- 小于
 *              gte或者ge -- 大于等于
 *              lte或者le -- 小于等于
 *        <{beginif}>
 *            <{if $varA ne "333"}>
 *                <span><{$varA}></span>
 *            <{/if}>
 *            <{if $varB eq aasf1}>
 *                <{$varB}>
 *            <{/if}>
 *            <{if $varA neq '666'}>
 *                asasaas
 *            <{/if}>
 *            <{else}>
 *                asdfsadf
 *            <{/else}>
 *        <{endif}>
 *
 *      9.模板foreach嵌套if
 *        <{foreach $varC as $b}>
 *            <{beginif}>
 *                <{if count($b) eq 标题1}>
 *                    <li><{$b}></li>
 *                <{/if}>
 *            <{endif}>
 *        <{endforeach}>
 *
 *      10.模板if嵌套foreach
 *        <{beginif}>
 *            <{if $varA ne "333"}>
 *                <{foreach $varC as $b}>
 *                    <span><{$varA}></span>
 *                <{endforeach}>
 *            <{/if}>
 *        <{endif}>
 *
 * 作者: pinyht
 * 版本: v0.1
 * 创建日期: 2012-04-20
 * 更新日期: 2012-05-03
 */

class Template{
    private $leftDelimiter;         // 左边界符,默认'<{'
    private $rightDelimiter;        // 右边界符,默认'}>'
    private $templateDir;           // 模板路径
    private $theme;                 // 主题风格名称,与主题路径名一致
    public $themeDir;              // 模板文件完整路径,包含主题路径
    private $dataArr;               // 以数组形式存储的模板数据集合
    private $tmpDataArr;            // 模板语句内部数据集,嵌套语句时使用

    /*
     * 类构造方法
     *
     * 功能说明: 对模板对象进行初始化,创建默认的属性值
     */
    public function __construct(){
        $this->leftDelimiter = '<{';        // 初始化左边界符
        $this->rightDelimiter = '}>';       // 初始化右边界符
        // 初始化模板路径,默认为web根目录下的template目录
        $this->templateDir = '../../template';  
        $this->theme = 'default';           // 初始化主题名称
        // 初始化主题完整路径
        $this->themeDir = rtrim($this->templateDir, DIRECTORY_SEPARATOR)
                          .'/'.$this->theme;  
        $this->dataArr = array();               // 初始化数据集
        $this->tmpDataArr = array();            // 初始化临时数据集
    }
    /*
     * 设定边界符
     *
     * 功能说明: 手动设置模板对象的左右边界符
     * 参数说明:
     *      $leftDelimiter -- 左边界符号
     *      $rightDelimiter -- 右边界符号
     */
    public function SetDelimiter($leftDelimiter = null, $rightDelimiter = null){
        if(isset($leftDelimiter)){
            $this->leftDelimiter = $leftDelimiter;
            $this->rightDelimiter = $rightDelimiter;
        }
    }
    /*
     * 设定模板路径
     *
     * 功能说明: 手动设置模板路径
     * 参数说明:
     *      $templateDir -- 模板所在位置的完整路径
     */
    public function SetTemplateDir($templateDir = null){
        if(isset($templateDir)){
            $this->templateDir = $templateDir;
        }
    }
    /*
     * 设定主题
     *
     * 功能说明: 手动设置主题
     * 参数说明:
     *      $templateDir -- 模板所在位置的完整路径
     */
    public function SetTheme($theme = null){
        if(isset($theme)){
            $this->theme = $theme;
            $this->themeDir = rtrim($this->templateDir, DIRECTORY_SEPARATOR)
                          .'/'.$this->theme;  
        }
    }
    /*
     * 收集数据
     *
     * 功能说明: 向模板对象收集数据,为输出模板做准备
     * 参数说明:
     *      $varName -- 变量名,在数据集中以数组的key形式存放,在模板文件中直接
     *                  通过变量名或数组调用
     *      $varValue -- 变量值,在数据集中以数组的value形式存放
     */
    public function Assign($varName, $varValue){
        if(isset($varName) && isset($varValue)){
            $this->dataArr[$varName] = $varValue;      // 将数据入栈
        }
    }
    /*
     * 收集临时数据
     *
     * 功能说明: 向模板对象收集临时数据,在模板语句解析过程中使用
     * 权限: 私有,只能类内部访问
     * 参数说明:
     *      $varName -- 变量名,在数据集中以数组的key形式存放,在模板文件中直接
     *                  通过变量名或数组调用
     *      $varValue -- 变量值,在数据集中以数组的value形式存放
     */
    private function AssignTmpData($varName, $varValue){
        if(isset($varName) && isset($varValue)){
            $this->tmpDataArr[$varName] = $varValue;      // 将数据入栈
        }
    }
    /*
     * 读取文件字符内容
     *
     * 功能说明: 读取指定模板文件的数据内容,以字符串形式返回页面html代码
     * 权限: 私有,只能类内部访问
     * 参数说明:
     *      $file -- 包含完整路径的模板文件名
     * 返回值:
     *      $fileContent -- 指定模板文件的html代码,以字符串形式返回
     */
    private function ReadFileContent($file){
        $fileResource = fopen($file, 'r');      // 打开文件,创建文件句柄
        // 读取整个文件内容
        $fileContent = fread($fileResource, filesize($file));   
        return $fileContent;
    }
    /*
     * 删除边界符
     *
     * 功能说明: 删除字符串中的模板左右边界符
     * 权限: 私有,只能类内部访问
     * 参数说明:
     *      $fileContent -- 需要转换的html代码
     * 返回值:
     *      $returnStr -- 转换过后的html代码
     */
    private function DeleteDelimiter($fileContent){
        if(isset($fileContent)){
            $returnStr;     // 返回的字符串
            // 删除左边界符
            $returnStr = ltrim($fileContent, $this->leftDelimiter);
            // 删除右边界符
            $returnStr = rtrim($returnStr, $this->rightDelimiter);
            return $returnStr;
        }
    }
    /*
     * 删除模板注释
     *
     * 功能说明: 删除模板语言中的注释语句
     * 权限: 私有,只能类内部访问
     * 参数说明:
     *      $fileContent -- 需要转换的html代码
     * 返回值:
     *      转换过后的html代码
     */
    private function DeleteComment($fileContent){
        if(isset($fileContent)){
            $returnStr;     // 返回的字符串
            // 正则表达式的(\r)?\n部分是为了删除代码中的回车换行符,避免出现
            // 空白行
            return preg_replace(
                '/'.$this->leftDelimiter.'\*.*\*'.$this->rightDelimiter
                .'(\r?\n)?/s', '', $fileContent);
        }
    }
    /*
     * 解析模板文件引入语句
     *
     * 功能说明: 解析模板文件引入语句,将引入的文件html代码加入到当前文件
     * 权限: 私有,只能类内部访问
     * 参数说明:
     *      $fileContent -- 需要转换的html代码
     * 返回值:
     *      转换过后的html代码
     */
    private function ConvertInclude($fileContent){
        if(isset($fileContent)){
            $returnStr = $fileContent;     // 返回的字符串
            // 筛选出include语句
            preg_match_all('/'.$this->leftDelimiter.'include\(.*\)'
                .$this->rightDelimiter.'/', $fileContent, $includeArr);
            // 循环遍历include语句,将指定的文件引入
            foreach($includeArr[0] as $includeVal){
                // 判断是否有重复的include语句,有就跳出本次循环,避免
                // 重复的include
                if(strpos($returnStr, $includeVal) === false){
                    continue;
                }
                // 删除左边多余部分
                $includeFile = ltrim($this->DeleteDelimiter($includeVal)
                                , 'include(');
                // 删除右边多余部分
                $includeFile = rtrim($includeFile, ')');
                // 去掉引号
                $includeFile = str_replace('\'', '', $includeFile);  
                $includeFile = str_replace('"', '', $includeFile);  
                // 替换include语句为指定的文件的html代码
                $returnStr = preg_replace('/'
                                .preg_quote($includeVal).'(\r?\n)?/'
                                , $this->ReadFileContent($this->themeDir
                                            .'/'.$includeFile)
                                , $returnStr);
            }
            return $returnStr;
        }
    }
    /*
     * 解析模板变量
     *
     * 功能说明: 解析模板语言中的直接变量输出语句
     * 权限: 私有,只能类内部访问
     * 参数说明:
     *      $fileContent -- 需要转换的html代码
     *      $tmpDataArr -- 需要指定处理的数据集,用于模板语句内部解析
     * 返回值:
     *      转换过后的html代码
     */
    private function ConvertVar($fileContent, $tmpDataArr=null){
        if(isset($fileContent)){
            // 如果指定了数据集,则比对指定的数据
            if(isset($tmpDataArr)){
                $dataArr = $tmpDataArr;
            }
            else{
                $dataArr = $this->dataArr;
            }
            // 匹配所有变量语句
            preg_match_all(
                '/'.$this->leftDelimiter.'\$.*'.$this->rightDelimiter.'/',
                $fileContent, $varArr);
            // 循环模板变量数组,判断变量在数据集中是否已经存在,存在则替换实际
            // 的值,将模板变量转换为数据
            foreach($varArr[0] as $value){
                $newValue = ltrim($this->DeleteDelimiter($value), '$');
                // 判断模板变量是取字符串还是数组的值
                $newValueArr = explode('.', $newValue);
                // 判断该模板变量在数据集中是否存在,不存在则不继续处理
                if(!isset($dataArr[$newValueArr[0]])){
                    continue;
                }
                $newValue = $dataArr[$newValueArr[0]];
                $newValueArrLen = count($newValueArr);
                // 循环取出数组类型的模板变量,找到真实的值
                for($i = 1;$i < $newValueArrLen; $i++){
                    // 进行多维数组的取值
                    $newValue = $newValue[$newValueArr[$i]];
                }
                if(isset($newValue)){
                    $fileContent = str_replace($value, $newValue, $fileContent);
                }
            }
            return $fileContent;     // 返回的字符串
        }
    }
    /*
     * 解析foreach循环
     *
     * 功能说明: 解析模板语言中的foreach语句,将循环体内的代码解析为相应的值
     * 权限: 私有,只能类内部访问
     * 参数说明:
     *      $fileContent -- 需要转换的html代码
     * 返回值:
     *      转换过后的html代码
     */
    private function ConvertForeach($fileContent){
        if(isset($fileContent)){
            // 匹配所有foreach语句块
            preg_match_all(
                '/'.$this->leftDelimiter.'foreach.*?endforeach'
                .$this->rightDelimiter.'/s', $fileContent, $foreachArr);
            // 循环模板foreach语句数组,判断语句在数据集中是否已经存在,存在则
            // 处理循环逻辑,将模板foreach转换为真实数据
            //print_r($foreachArr[0]);exit;
            foreach($foreachArr[0] as $value){
                // 判断是否有重复的foreach语句,有就跳出本次循环,避免
                // 重复的解析
                if(strpos($fileContent, $value) === false){
                    continue;
                }
                $foreachValue = $value;     // 暂存原始的模板foreach语句
                $newForeachValue = '';      // 需要替换foreach语句块的html
                // 取出foreach开头语句
                preg_match_all('/'.$this->leftDelimiter.'foreach.*'
                    .$this->rightDelimiter.'/', $value, $foreachStart);
                if(isset($foreachStart[0][0])){
                    // 取出foreach内部代码块
                    $foreachHtml = str_replace($foreachStart[0][0], '', $value);
                    $foreachHtml = str_replace($this->leftDelimiter.'endforeach'
                                    .$this->rightDelimiter, '', $foreachHtml);
                    // 去掉开头或结尾的回车符
                    $foreachHtml = preg_replace('/(^(\r?\n)?|(\r?\n)?$)/s'
                                    , '', $foreachHtml);
                    // 删除foreach开头语句的标识部分
                    $foreachStart = ltrim($foreachStart[0][0]
                                        , $this->leftDelimiter.'foreach');
                    $foreachStart = rtrim($foreachStart
                                        , $this->rightDelimiter);
                    // 按as分割语句,取出需要的变量
                    $tmpArr = explode('as', $foreachStart);
                    if(!isset($tmpArr[0]) && !isset($tmpArr[1])){
                        continue;
                    }
                    // 如果数据集里没有foreach指定的值则不解析
                    if(!isset($this->dataArr[ltrim(trim($tmpArr[0]), '$')])){
                        continue;
                    }
                    // 取出数组值
                    $fcArr = $this->dataArr[ltrim(trim($tmpArr[0]), '$')];
                    // 取出遍历指代变量名
                    $fcVarName = trim($tmpArr[1]);
                    $fcVarName = ltrim($fcVarName, '$');
                    // 清空临时数据集
                    $this->InitTmpData();
                    $newForeachValue = '';  // 替换代码块的html
                    // 循环取出需要的值
                    foreach($fcArr as $value){
                        $this->AssignTmpData($fcVarName, $value);
                        // 转换foreach内部if语句
                        $newForeachHTML = $this->ConvertIf($foreachHtml
                                            , $this->tmpDataArr);
                        // 转换foreach内部模板变量
                        $newForeachHTML = $this->ConvertVar($newForeachHTML
                                            , $this->tmpDataArr);
                        $newForeachValue .= $newForeachHTML."\n";     // 实际输出的html
                    }
                    if($newForeachValue  != ''){
                        // 如果有新内容出现就替换原始foreach代码块
                        $fileContent = str_replace($foreachValue
                                        , rtrim($newForeachValue)
                                        , $fileContent);
                    }
                }
            }
            return $fileContent;     // 返回的字符串
        }
    }
    /*
     * 解析if判断语句
     *
     * 功能说明: 解析模板语言中的if语句
     * 权限: 私有,只能类内部访问
     * 参数说明:
     *      $fileContent -- 需要转换的html代码
     *      $tmpDataArr -- 需要指定处理的数据集,用于模板语句内部解析
     * 返回值:
     *      转换过后的html代码
     */
    private function ConvertIf($fileContent, $tmpDataArr=null){
        if(isset($fileContent)){
            // 如果指定了数据集,则比对指定的数据
            if(isset($tmpDataArr)){
                $dataArr = $tmpDataArr;
            }
            else{
                $dataArr = $this->dataArr;
            }
            // 匹配所有if语句块
            preg_match_all(
                '/'.$this->leftDelimiter.'beginif.*?endif'
                .$this->rightDelimiter.'/s', $fileContent, $ifArr);
            // 循环模板if语句数组,判断语句在数据集中是否已经存在,存在则处理
            // 逻辑循环,将模板if语句解析为真实语句
            foreach($ifArr[0] as $value){
                // 判断是否有重复的if语句,有就跳出本次循环,避免
                // 重复的解析
                if(strpos($fileContent, $value) === false){
                    continue;
                }
                $tmpIfValue = $value;   // 记录下初始的if语句块
                $outputArr = array();   // 需要替换的html代码数组
                // 匹配所有可能if语句
                preg_match_all('/'.$this->leftDelimiter
                    .'if.*?\/if'.$this->rightDelimiter.'/s', $value
                    , $tmpIfArr);
                if(!$tmpIfArr[0]){
                    continue;
                }
                // 循环每个if语句里面的子判断
                foreach($tmpIfArr[0] as $value){
                    $tmpValue = $value;     // 记录下初始的子判断语句
                    // 匹配条件判断部分
                    preg_match('/'.$this->leftDelimiter.'if.*?'
                        .$this->rightDelimiter.'/s', $value, $tmpExp);
                    if(!$tmpExp[0]){
                        continue;
                    }
                    // 删选出判断条件
                    $tmpExp = preg_replace('/'.$this->leftDelimiter
                                .'if/', '', $tmpExp[0]);
                    $tmpExp = rtrim($tmpExp, $this->rightDelimiter);
                    // 按空格分隔取出条件部分
                    $tmpExpArr = explode(' ', $tmpExp);
                    $expArr = array();
                    foreach($tmpExpArr as $value){
                        if($value != ''){
                            array_push($expArr, $value);
                        }
                    }
                    if(count($expArr) != 3){
                        continue;
                    }
                    // 判断if里面的计算条件
                    if($expArr[1] == 'eq'){
                        // 等于
                        if($dataArr[ltrim($expArr[0], '$')]
                            == $expArr[2]){
                            // 条件成立,取出判断条件内部的html代码
                            $expContent = preg_replace('/'.$this->leftDelimiter
                                .'if.*?'.$this->rightDelimiter.'(\r?\n)?/', ''
                                , $tmpValue);
                            $expContent = str_replace($this->leftDelimiter
                                            .'/if'.$this->rightDelimiter
                                            , '', $expContent);
                            // 转换foreach语句
                            $expContent = $this->ConvertForeach($expContent);
                            // 转换模板变量
                            $expContent = $this->ConvertVar($expContent,$dataArr);
                            array_push($outputArr, $expContent);
                        }
                    }
                    else if($expArr[1] == 'ne' || $expArr[1] == 'neq'){
                        // 不等于
                        if($dataArr[ltrim($expArr[0], '$')]
                            != $expArr[2]){
                            // 条件成立,取出判断条件内部的html代码
                            $expContent = preg_replace('/'.$this->leftDelimiter
                                .'if.*?'.$this->rightDelimiter.'(\r?\n)?/', ''
                                , $tmpValue);
                            $expContent = str_replace($this->leftDelimiter
                                            .'/if'.$this->rightDelimiter
                                            , '', $expContent);
                            // 转换foreach语句
                            $expContent = $this->ConvertForeach($expContent);
                            // 转换模板变量
                            $expContent = $this->ConvertVar($expContent);
                            array_push($outputArr, $expContent);
                        }
                    }
                    else if($expArr[1] == 'gt'){
                        // 大于 
                        if($dataArr[ltrim($expArr[0], '$')]
                            > $expArr[2]){
                            // 条件成立,取出判断条件内部的html代码
                            $expContent = preg_replace('/'.$this->leftDelimiter
                                .'if.*?'.$this->rightDelimiter.'(\r?\n)?/', ''
                                , $tmpValue);
                            $expContent = str_replace($this->leftDelimiter
                                            .'/if'.$this->rightDelimiter
                                            , '', $expContent);
                            // 转换foreach语句
                            $expContent = $this->ConvertForeach($expContent);
                            // 转换模板变量
                            $expContent = $this->ConvertVar($expContent);
                            array_push($outputArr, $expContent);
                        }
                    }
                    else if($expArr[1] == 'lt'){
                        // 小于 
                        if($dataArr[ltrim($expArr[0], '$')]
                            < $expArr[2]){
                            // 条件成立,取出判断条件内部的html代码
                            $expContent = preg_replace('/'.$this->leftDelimiter
                                .'if.*?'.$this->rightDelimiter.'(\r?\n)?/', ''
                                , $tmpValue);
                            $expContent = str_replace($this->leftDelimiter
                                            .'/if'.$this->rightDelimiter
                                            , '', $expContent);
                            // 转换foreach语句
                            $expContent = $this->ConvertForeach($expContent);
                            // 转换模板变量
                            $expContent = $this->ConvertVar($expContent);
                            array_push($outputArr, $expContent);
                        }
                    }
                    else if($expArr[1] == 'gte' || $expArr[1] == 'ge'){
                        // 大于等于 
                        if($dataArr[ltrim($expArr[0], '$')]
                            >= $expArr[2]){
                            // 条件成立,取出判断条件内部的html代码
                            $expContent = preg_replace('/'.$this->leftDelimiter
                                .'if.*?'.$this->rightDelimiter.'(\r?\n)?/', ''
                                , $tmpValue);
                            $expContent = str_replace($this->leftDelimiter
                                            .'/if'.$this->rightDelimiter
                                            , '', $expContent);
                            // 转换foreach语句
                            $expContent = $this->ConvertForeach($expContent);
                            // 转换模板变量
                            $expContent = $this->ConvertVar($expContent);
                            array_push($outputArr, $expContent);
                        }
                    }
                    else if($expArr[1] == 'lte' || $expArr[1] == 'le'){
                        // 小于等于 
                        if($dataArr[ltrim($expArr[0], '$')]
                            <= $expArr[2]){
                            // 条件成立,取出判断条件内部的html代码
                            $expContent = preg_replace('/'.$this->leftDelimiter
                                .'if.*?'.$this->rightDelimiter.'(\r?\n)?/', ''
                                , $tmpValue);
                            $expContent = str_replace($this->leftDelimiter
                                            .'/if'.$this->rightDelimiter
                                            , '', $expContent);
                            // 转换foreach语句
                            $expContent = $this->ConvertForeach($expContent);
                            // 转换模板变量
                            $expContent = $this->ConvertVar($expContent);
                            array_push($outputArr, $expContent);
                        }
                    }
                }
                // 处理最后的else条件体,只有当所有if语句都不成立时才执行
                if(!isset($outputArr[0])){
                    // 取出else部分
                    preg_match_all('/'.$this->leftDelimiter
                        .'else.*?\/else'.$this->rightDelimiter.'/s'
                        , $tmpIfValue, $tmpElse);
                    if(isset($tmpElse[0][0])){
                        $tmpElse = preg_replace('/'.$this->leftDelimiter
                                        .'\/?else'.$this->rightDelimiter
                                        .'(\r?\n)?/', '', $tmpElse[0][0]);
                        // 处理foreach部分
                        $tmpElse = $this->ConvertForeach($tmpElse);
                        $tmpElse = $this->ConvertVar($tmpElse);
                        array_push($outputArr, $tmpElse);
                    }
                }

                // 替换原始的模板代码
                $newIfHTML = '';
                foreach($outputArr as $value){
                    $newIfHTML .= trim($value)."\n"; 
                }

                // 替换原始的if代码块
                $fileContent = str_replace($tmpIfValue
                                , rtrim($newIfHTML, "\n")
                                , $fileContent);
            }
            return $fileContent;
        }
    }
    /*
     * 清空临时数据集
     *
     * 功能说明: 清空数据集数据,避免模板内部语句处理错误
     * 权限: 私有,只能类内部访问
     */
    private function InitTmpData(){
        $this->tmpDataArr = array();
    }
    /*
     * 输出数据
     *
     * 功能说明: 转换指定模板文件,将数据集的内容与模板文件关联并想浏览器输出转化
     *           后的模板文件
     * 参数说明:
     *      $fileName -- 模板文件名,不包含路径
     */
    public function Display($fileName){
        if(isset($fileName)){
            $file = $this->themeDir.'/'.$fileName;      // 获取模板文件完整路径
            $fileContent = $this->ReadFileContent($file);   // 读取模板文件数据
            $fileContent = $this->DeleteComment($fileContent);  // 删除注释语句
            $fileContent = $this->ConvertInclude($fileContent);  // 转换包含语句
            // 转换foreach语句
            $fileContent = $this->ConvertForeach($fileContent);  
            // 转换if语句
            $fileContent = $this->ConvertIf($fileContent);  
            $fileContent = $this->ConvertVar($fileContent);  // 转换变量输出语句
            echo $fileContent;      // 输出模板内容
            //$this->InitData();      // 清空数据集
        }
    }
}

?>
