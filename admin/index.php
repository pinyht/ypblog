<?php
/*
 * ------------------------------------------------------------
 * 后台入口文件
 *
 * 功能说明: 进行系统初始化和模块路由
 * 作者: ypblog
 * 版本: v0.1
 * 创建日期: 2012-05-08
 * 更新日期: 2012-05-08
 * 版权信息: Copyright (c) 2012, pinyht   
 * ------------------------------------------------------------
 */

// 加载基础配置文件
require_once('../config/config.common.php');

// 加载数据库相关配置
require_once('../config/config.mysql.php');

// 加载类文件自动包含函数
require_once('../kernel/class/init_class.php');

// 初始化pdo对象
$pdo = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASSWORD);

// 初始化模板对象
$template = new Template();
$template->SetDelimiter('<{', '}>');
$template->SetTemplateDir(ROOT_PATH.'/admin/template');
$template->SetTheme('default');

// 定义模板公共数据
$siteConfig = array(
    'path' => SITE_PATH.'/admin/template/default/',   // 定义模板路径
    'charset' => 'utf-8',            // 定义模板编码
    'title' => 'YPblog',             // 定义模板title
);
$template->Assign('siteConfig', $siteConfig);

session_start();    // 开启会话
// 判断是否已经登录
if(!isset($_SESSION['admin_logined'])){
    // 未登录则显示登录页面
    include_once(ROOT_PATH.'/admin/login.php');
}
else{
    
}
?>
